/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.pokemon.midterm;

/**
 *
 * @author Admin
 */
public class Pikachu extends Pokemon {
    private Fox fox;

    public Pikachu(int x, int y, int power, int hp, char symbol) {
        super(x, y, power, hp, symbol);
    }

    public boolean attack() {
        if (this.x != fox.x) {
            System.out.println(pikachuMissed());
            this.hp -= fox.power;
        } else {
            System.out.print(nameSkill());
            fox.hp -= this.power;
        }
        System.out.println(" ");
        return true;
    }

    public boolean attack(char leftOrRight, int step) {
        switch (leftOrRight) {
            case 'L':
            case 'l':
                x = x - step;
                if (getX() == fox.x) {
                    System.out.print(nameSkill());
                    fox.hp -= this.power/2;
                } else {
                    System.out.println(pikachuMissed());
                    this.hp -= fox.power;
                }
                break;
            case 'R':
            case 'r':
                x = x + step;
                if (getX() == fox.x) {
                    System.out.print(nameSkill());
                    fox.hp -= this.power/2;
                } else {
                    System.out.println(pikachuMissed());
                    this.hp -= fox.power;
                }
                break;
        }
        System.out.println(" ");
        return true;
    }
    


    public void setFox(Fox fox) {
        this.fox = fox;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public String pikachuMissed() {
        return "Pikachu: Missed!!! \nFox: Counter attack!!!";
    }
    
    @Override
    public String nameSkill() {
        return "Pikachu: Thunder Shock!!! \n";
    }
}
