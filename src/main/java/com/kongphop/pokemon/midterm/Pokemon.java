/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.pokemon.midterm;

/**
 *
 * @author Admin
 */
public class Pokemon {
    protected int x;
    protected int y;
    protected int power;
    protected int hp;
    protected char symbol;
    
    public Pokemon(int x, int y, int power,  int hp, char symbol){
        this.x = x;
        this.y = y;
        this.power = power;
        this.hp = hp;
        this.symbol = symbol;
        
    }  
    
    public char getSymbol() {
        return symbol;
    }
    
    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
    
    public String nameSkill() {
        return "Pokemon: Punch!!!";
    }
}
