/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.pokemon.midterm;

/**
 *
 * @author Admin
 */
public class Arena {
    private int width;
    private int height;
    private Fox fox;
    private Pikachu pikachu;
    
    public Arena(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public void showArena() {
        
        for (int y = 0; y < height; y++) {   
            for (int x = 0; x < width; x++) {
                if(pikachu.isOn(x, y)){
                    System.out.print(pikachu.getSymbol());
                } else if(fox.isOn(x, y)){
                    System.out.print(fox.getSymbol());
                }else{
                    if(y != 1){
                        System.out.print("-");
                    }
                }
            }
            System.out.println(" ");
        }
    }
    
    public void setFox(Fox fox) {
        this.fox = fox;
    }
    
    public void setPikachu(Pikachu pikachu) {
        this.pikachu = pikachu;
    }
}
