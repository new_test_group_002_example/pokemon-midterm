/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.pokemon.midterm;

/**
 *
 * @author Admin
 */
public class Battle {
    public static void main(String[] args) {
        Arena arena = new Arena(10, 3);
        Fox fox = new Fox(3, 0, 15, 60, 'F');
        Pikachu pikachu = new Pikachu(6, 2, 20, 40, 'P');
        arena.setPikachu(pikachu);
        arena.setFox(fox);
        pikachu.setFox(fox);
        
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack();
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack('R', 1);
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack('L', 4);
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack();
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack();
        arena.showArena();
        battleStatus(fox, pikachu);
        
        pikachu.attack();
        arena.showArena();
        battleStatus(fox, pikachu); 
    }
    
    public static void battleStatus(Fox fox, Pikachu pikachu) {
        foxStatus(fox);
        pikachuStatus(pikachu);
        lineNewRound();
        battleResult(fox, pikachu);
    }
    
    public static void lineNewRound() {
        System.out.println("-----------------------------------");
    }

    public static void battleResult(Fox fox, Pikachu pikachu) {
        if(fox.hp <= 0) {
            printWin();
        } else if(pikachu.hp <= 0){
            printLose();
        }
    }

    public static void pikachuStatus(Pikachu pikachu) {
        System.out.println("Pikachu status-> Power: "
                +pikachu.power+" / HP: "+pikachu.hp);
        System.out.println("At ("+pikachu.x+", "+pikachu.y+")");
    }

    public static void foxStatus(Fox fox) {
        System.out.println("Fox status-> Power: "
                +fox.power+" / HP: "+fox.hp);
        System.out.println("At ("+fox.x+", "+fox.y+")");
    }
    
    private static void printWin() {
        System.out.println("Pikachu WIN!!!");
    }
    
    private static void printLose() {
        System.out.println("Pikachu LOSE!!!");
    }
}
